# PropShop Ecommerce Website With Django + React

## Architecture

![Architecture](/app/static/images/architecture.png)

## HomeScreen

![HomeScreen](/app/static/images/HomeScreen.png)

## OrderSummary

![OrderSummary](/app/static/images/OrderSummary.png)
