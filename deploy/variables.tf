variable "prefix" {
  type        = string
  default     = "psa"
  description = "pro-shop-app"
}

variable "project" {
  default = "pro-shop-app"
}

variable "contact" {
  default = "da1kun@example.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "pro-shop-app-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "669987674182.dkr.ecr.us-east-1.amazonaws.com/pro-shop-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "669987674182.dkr.ecr.us-east-1.amazonaws.com/pro-shop-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "da1kunappdev.co.uk"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = ""
    staging    = "staging."
    dev        = "dev."
  }
}

