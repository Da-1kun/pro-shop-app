terraform {
  backend "s3" {
    bucket         = "pro-shop-app-tfstate"
    key            = "pro-shop-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "pro-shop-app-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.33.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
