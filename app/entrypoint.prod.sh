#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z $DB_HOST 5432; do
    sleep 0.1
done

echo "PostgreSQL started"

python manage.py migrate
python manage.py loaddata api/fixtures/data.json
python manage.py collectstatic --no-input --clear
gunicorn config.wsgi:application --bind 127.0.0.1:9000

exec "$@"